//
//  appApp.swift
//  app
//
//  Created by Билет в будущее on 9/2/2024.
//

import SwiftUI

@main
struct appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

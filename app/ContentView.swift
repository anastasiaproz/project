//
//  ContentView.swift
//  app
//
//  Created by Билет в будущее on 9/2/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            
        }
        .padding()
    }
}

#Preview {
    Home()
}

struct Home : View{
    var body: some View{
        
        ZStack{
            Color("Color")
                .ignoresSafeArea()
            VStack{
                VStack{
                    HStack{
                        Image("Image1")
                            .resizable()
                            .frame(width: 50, height: 60)
                        Text("GoRacing")
                            .font(.title)
                            .fontWeight(.medium)
                            .foregroundColor(.white)
                    }
                    
                    Spacer()
                }
                .padding(.top, 30)
                
                VStack(spacing: 15){
                    
                    Button(action: {
                        
                    }) {
                        Text("Start a new game")
                            .foregroundColor(.black)
                            .padding(20)
                    }
                    .padding(.horizontal, 100)
                    .background(Color(.yellow))
                    .cornerRadius(10)
                    
                    
                    
                     Button(action: {
                    
                    }) {
                        Text("Results")
                            .foregroundColor(.black)
                            .padding(20)
                                       } 
                    .padding(.horizontal, 138)
                        .background(Color(.yellow))
                        .cornerRadius(10)
                        
                                        
                    Button(action: {
                    
                    }) {
                        Text("Settings")
                            .foregroundColor(.black)
                            .padding(20)
                                       }
                    .padding(.horizontal, 135)
                        .background(Color(.yellow))
                        .cornerRadius(10)
                                        
                    Button(action: {
                                           
                    }) {
                        
                        Text("Exit") 
                            .foregroundColor(.black)
                            .padding(20)
                        
                    } .padding(.horizontal, 151)
                        .background(Color(.red))
                        .cornerRadius(10)
                        .padding(.bottom, 100)
                    Spacer()
                    Spacer()
                    Spacer()
                    Spacer()
                }
            }
        }
    }
}

struct Game : View{
    var body: some View{
        
        ZStack{
            
            
        }
        
    }
}
